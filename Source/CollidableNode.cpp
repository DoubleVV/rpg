//
// Created by DoubleVV on 18/01/2017.
//

#include "../Include/CollidableNode.hpp"

CollidableNode::CollidableNode(Category::Type category, const sf::Vertex* quad)
        : mRect(quad[0].position, quad[2].position - quad[0].position),
          mCategory(category) {
    setPosition(mRect.left, mRect.top);
    
}

CollidableNode::CollidableNode(Category::Type category, const sf::FloatRect rect)
        : mRect(rect),
          mCategory(category) {
    setPosition(mRect.left, mRect.top);
}


sf::FloatRect CollidableNode::getBoundingRect() const{
    return mRect;
}

unsigned int CollidableNode::getCategory() const{
    return mCategory;
}

