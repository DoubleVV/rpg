//
// Created by DoubleVV on 10/08/2016.
//

#include "../Include/DataTables.hpp"

using namespace std::placeholders;

std::vector<TileData> initializeTileData(){
    std::vector<TileData> data(Tiles::TypeCount);

    data[Tiles::Empty].texture = Textures::TileSet;
    data[Tiles::Empty].textureRect = sf::IntRect(1000,1000,16,16);
    data[Tiles::Empty].isEmpty = true;
    data[Tiles::Empty].animationFrame = 1;

    data[Tiles::Grass1].texture = Textures::TileSet;
    data[Tiles::Grass1].textureRect = sf::IntRect(640,0,32,32);
    data[Tiles::Grass1].isEmpty = true;
    data[Tiles::Grass1].animationFrame = 1;
    
    data[Tiles::Grass2].texture = Textures::TileSet;
    data[Tiles::Grass2].textureRect = sf::IntRect(656,0,16,16);
    data[Tiles::Grass2].isEmpty = true;
    data[Tiles::Grass2].animationFrame = 2;
    
    data[Tiles::Grass3].texture = Textures::TileSet;
    data[Tiles::Grass3].textureRect = sf::IntRect(640,16,16,16);
    data[Tiles::Grass3].isEmpty = true;
    data[Tiles::Grass3].animationFrame = 1;
    
    data[Tiles::Grass4].texture = Textures::TileSet;
    data[Tiles::Grass4].textureRect = sf::IntRect(656,16,16,16);
    data[Tiles::Grass4].isEmpty = true;
    data[Tiles::Grass4].animationFrame = 10;

    data[Tiles::Water1].texture = Textures::TileSet;
    data[Tiles::Water1].textureRect = sf::IntRect(832,96,16,16);
    data[Tiles::Water1].isEmpty = true;
    data[Tiles::Water1].animationFrame = 10;

    data[Tiles::Water2].texture = Textures::TileSet;
    data[Tiles::Water2].textureRect = sf::IntRect(848,96,16,16);
    data[Tiles::Water2].isEmpty = true;
    data[Tiles::Water2].animationFrame = 10;

    data[Tiles::Water3].texture = Textures::TileSet;
    data[Tiles::Water3].textureRect = sf::IntRect(832,112,16,16);
    data[Tiles::Water3].isEmpty = true;
    data[Tiles::Water3].animationFrame = 10;

    data[Tiles::Water4].texture = Textures::TileSet;
    data[Tiles::Water4].textureRect = sf::IntRect(848,112,16,16);
    data[Tiles::Water4].isEmpty = true;
    data[Tiles::Water4].animationFrame = 10;

    data[Tiles::Water5].texture = Textures::TileSet;
    data[Tiles::Water5].textureRect = sf::IntRect(848,160,16,16);
    data[Tiles::Water5].isEmpty = true;
    data[Tiles::Water5].animationFrame = 10;

    data[Tiles::GrassWater1].texture = Textures::TileSet;
    data[Tiles::GrassWater1].textureRect = sf::IntRect(832,128,16,16);
    data[Tiles::GrassWater1].isEmpty = false;
    data[Tiles::GrassWater1].animationFrame = 10;

    data[Tiles::GrassWater2].texture = Textures::TileSet;
    data[Tiles::GrassWater2].textureRect = sf::IntRect(832,144,16,16);
    data[Tiles::GrassWater2].isEmpty = false;
    data[Tiles::GrassWater2].animationFrame = 10;

    data[Tiles::GrassWater3].texture = Textures::TileSet;
    data[Tiles::GrassWater3].textureRect = sf::IntRect(832,160,16,16);
    data[Tiles::GrassWater3].isEmpty = false;
    data[Tiles::GrassWater3].animationFrame = 10;

    data[Tiles::GrassWater4].texture = Textures::TileSet;
    data[Tiles::GrassWater4].textureRect = sf::IntRect(832,176,16,16);
    data[Tiles::GrassWater4].isEmpty = false;
    data[Tiles::GrassWater4].animationFrame = 10;

    data[Tiles::WaterGrass1].texture = Textures::TileSet;
    data[Tiles::WaterGrass1].textureRect = sf::IntRect(880,128,16,16);
    data[Tiles::WaterGrass1].isEmpty = false;
    data[Tiles::WaterGrass1].animationFrame = 10;

    data[Tiles::WaterGrass2].texture = Textures::TileSet;
    data[Tiles::WaterGrass2].textureRect = sf::IntRect(880,144,16,16);
    data[Tiles::WaterGrass2].isEmpty = false;
    data[Tiles::WaterGrass2].animationFrame = 10;

    data[Tiles::WaterGrass3].texture = Textures::TileSet;
    data[Tiles::WaterGrass3].textureRect = sf::IntRect(880,160,16,16);
    data[Tiles::WaterGrass3].isEmpty = false;
    data[Tiles::WaterGrass3].animationFrame = 10;

    data[Tiles::WaterGrass4].texture = Textures::TileSet;
    data[Tiles::WaterGrass4].textureRect = sf::IntRect(880,176,16,16);
    data[Tiles::WaterGrass4].isEmpty = false;
    data[Tiles::WaterGrass4].animationFrame = 10;

    data[Tiles::GrassCliff1].texture = Textures::TileSet;
    data[Tiles::GrassCliff1].textureRect = sf::IntRect(672,192,16,16);
    data[Tiles::GrassCliff1].isEmpty = true;
    data[Tiles::GrassCliff1].animationFrame = 10;

    data[Tiles::GrassCliff2].texture = Textures::TileSet;
    data[Tiles::GrassCliff2].textureRect = sf::IntRect(688,192,16,16);
    data[Tiles::GrassCliff2].isEmpty = true;
    data[Tiles::GrassCliff2].animationFrame = 10;

    data[Tiles::Cliff1].texture = Textures::TileSet;
    data[Tiles::Cliff1].textureRect = sf::IntRect(704,192,16,16);
    data[Tiles::Cliff1].isEmpty = false;
    data[Tiles::Cliff1].animationFrame = 10;

    data[Tiles::Cliff2].texture = Textures::TileSet;
    data[Tiles::Cliff2].textureRect = sf::IntRect(720,192,16,16);
    data[Tiles::Cliff2].isEmpty = false;
    data[Tiles::Cliff2].animationFrame = 10;

    data[Tiles::Cliff3].texture = Textures::TileSet;
    data[Tiles::Cliff3].textureRect = sf::IntRect(704,208,16,16);
    data[Tiles::Cliff3].isEmpty = false;
    data[Tiles::Cliff3].animationFrame = 10;

    data[Tiles::Cliff4].texture = Textures::TileSet;
    data[Tiles::Cliff4].textureRect = sf::IntRect(720,208,16,16);
    data[Tiles::Cliff4].isEmpty = false;
    data[Tiles::Cliff4].animationFrame = 10;
    
    return data;
}

std::vector<MapData> initializeMapData(){
    std::vector<MapData> data(Maps::MapCount);

    data[Maps::Castle1].path = "Media/Maps/castle1.json";
    data[Maps::Castle1].texture = Textures::TileSet;

    data[Maps::Test].path = "Media/Maps/test3.json";
    data[Maps::Test].texture = Textures::TileSet;

    data[Maps::Forest1].path = "Media/Maps/forest1.json";
    data[Maps::Forest1].texture = Textures::TileSet;

    data[Maps::Forest2].path = "Media/Maps/forest2.json";
    data[Maps::Forest2].texture = Textures::TileSet;

    data[Maps::Village1].path = "Media/Maps/village1.json";
    data[Maps::Village1].texture = Textures::TileSet;

    data[Maps::Village2].path = "Media/Maps/village2.json";
    data[Maps::Village2].texture = Textures::TileSet;

    return data;
}