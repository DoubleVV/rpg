//
// Created by DoubleVV on 26/07/2016.
//

#include <SFML/Graphics/RectangleShape.hpp>
#include "../Include/GameState.hpp"
#include "../Include/Utility.hpp"

GameState::GameState(StateStack& stack, Context context)
        : State(stack, context),
          mWorld(*context.window, *context.fonts),
          mPlayer(*context.player),
          mHealthDisplay(),
          mDebugPosition() {
    mPlayer.setMissionStatus(Player::MissionFailure);

    sf::RenderWindow& window = *context.window;
    sf::Vector2f windowSize(context.window->getSize());

    sf::Font& font = context.fonts->get(Fonts::Main);
    sf::Font& stat = context.fonts->get(Fonts::Stats);

    mHealthDisplay.setFont(font);
    mHealthDisplay.setCharacterSize(20u);
    mHealthDisplay.setString("0/0");
    centerOrigin(mHealthDisplay);
    mHealthDisplay.setPosition(windowSize.x * 0.5f, windowSize.y * 0.9f);

    mDebugPosition.setFont(stat);
    mDebugPosition.setCharacterSize(10u);
    mDebugPosition.setPosition(5.f, 15.f);
}

bool GameState::handleEvent(const sf::Event& event) {
    CommandQueue& commands = mWorld.getCommandQueue();
    mPlayer.handleEvent(event, commands);
    
//    if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
//        requestStackPush(States::Pause);
    
//    mPlayer.handleRealTimeInput(commands);
    
    return true;
}

bool GameState::update(sf::Time deltaTime) {
    mWorld.update(deltaTime);
    
    if(!mWorld.hasAlivePlayer()){
        mPlayer.setMissionStatus(Player::MissionFailure);
        requestStackPush(States::GameOver);
    }
    
    CommandQueue& commands = mWorld.getCommandQueue();
    mPlayer.handleRealTimeInput(commands);

    updateHud();
    
    return true;
}

void GameState::draw() {
    mWorld.draw();

    sf::RenderWindow& window = *getContext().window;
    window.setView(window.getDefaultView());

    window.draw(mHealthDisplay);
    window.draw(mDebugPosition);
}

void GameState::updateHud() {
    mHealthDisplay.setString(toString(mWorld.getPlayerCharacterHealth()) + "/" + toString(mWorld.getPlayerCharacterMaxHealth()));
    mDebugPosition.setString(toString((int)mWorld.getPlayerCharacterPosition().x) + "," + toString((int)mWorld.getPlayerCharacterPosition().y));
}
