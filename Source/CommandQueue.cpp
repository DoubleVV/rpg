//
// Created by DoubleVV on 31/07/2016.
//

#include "../Include/CommandQueue.hpp"
#include "../Include/SceneNode.hpp"

void CommandQueue::push(const Command &command) {
    mQueue.push(command);
}

Command CommandQueue::pop() {
    Command command = mQueue.front();
    mQueue.pop();
    return command;
}

bool CommandQueue::isEmpty() const {
    return mQueue.empty();
}

