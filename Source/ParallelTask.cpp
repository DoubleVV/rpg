//
// Created by DoubleVV on 04/08/2016.
//

#include "../Include/ParallelTask.hpp"

bool ParallelTask::isFinished() {
    sf::Lock lock(mMutex);
    return mFinished;
}

void ParallelTask::runTask() {
    bool ended = false;
    while (!ended){
        sf::Lock lock(mMutex);
        if(mElapsedTime.getElapsedTime().asSeconds() >=5.f)
            ended = true;
    }
    
    {
        sf::Lock lock(mMutex);
        mFinished = true;
    }
    
}

ParallelTask::ParallelTask()
        : mThread(&ParallelTask::runTask, this),
          mFinished(false) {
    
}

void ParallelTask::execute() {
    mFinished = false;
    mElapsedTime.restart();
    mThread.launch();
}

float ParallelTask::getCompletion() {
    sf::Lock lock(mMutex);
    
    return mElapsedTime.getElapsedTime().asSeconds()/5.f;
}
