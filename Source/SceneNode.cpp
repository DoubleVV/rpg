//
// Created by DoubleVV on 27/07/2016.
//

#include <c++/algorithm>
#include <c++/cassert>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "../Include/SceneNode.hpp"
#include "../Include/Command.hpp"
#include "../Include/Utility.hpp"

SceneNode::SceneNode(Category::Type category)
        : mChildren(),
          mParent(nullptr),
          mDefaultCategory(category) {
    
}

void SceneNode::attachChild(Ptr child) {
    child->mParent = this;
    mChildren.push_back(std::move(child));
}

SceneNode::Ptr SceneNode::detachChild(const SceneNode &node) {
    auto found = std::find_if(mChildren.begin(), mChildren.end(),
                              [&] (Ptr &p) -> bool { return p.get() == &node;});
    
    assert(found != mChildren.end());
    
    Ptr result = std::move(*found);
    result->mParent = nullptr;
    mChildren.erase(found);
    return result;
}

void SceneNode::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();
    
    drawCurrent(target, states);
    drawChildren(target, states);
    drawBoundingRect(target, states);
}

void SceneNode::drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const {
    
}

void SceneNode::drawChildren(sf::RenderTarget &target, sf::RenderStates states) const {
    for(const Ptr& child : mChildren)
        child->draw(target, states);
}


void SceneNode::update(sf::Time dt, CommandQueue& commands) {
    updateCurrent(dt, commands);
    updateChildren(dt, commands);
}

void SceneNode::updateCurrent(sf::Time dt, CommandQueue& commands) {
    
}

void SceneNode::updateChildren(sf::Time dt, CommandQueue& commands) {
    for(Ptr& child : mChildren)
        child->update(dt, commands);
}

sf::Transform SceneNode::getWorldTransform() const {
    sf::Transform transform = sf::Transform::Identity;
    for(const SceneNode* node = this; node!= nullptr; node = node->mParent)
        transform = node->getTransform() * transform;
    
    return transform;
}

sf::Vector2f SceneNode::getWorldPosition() const {
    return getWorldTransform() * sf::Vector2f();
}

unsigned int SceneNode::getCategory() const {
    return mDefaultCategory;
}

void SceneNode::onCommand(const Command &command, sf::Time dt) {
    if(command.category & getCategory())
        command.action(*this, dt);
    
    for(Ptr& child : mChildren)
        child->onCommand(command, dt);
}

void SceneNode::drawBoundingRect(sf::RenderTarget &target, sf::RenderStates states) const {
    sf::FloatRect rect = getBoundingRect();
    
    sf::RectangleShape shape;
    shape.setPosition(sf::Vector2f(rect.left, rect.top));
    shape.setSize(sf::Vector2f(rect.width, rect.height));
    shape.setFillColor(sf::Color::Transparent);
    shape.setOutlineColor(sf::Color::Green);
    shape.setOutlineThickness(1.f);
    
    target.draw(shape);
}

sf::FloatRect SceneNode::getBoundingRect() const {
    return sf::FloatRect();
}

void SceneNode::checkSceneCollision(SceneNode &sceneGraph, std::set<SceneNode::Pair> &collisionPairs, sf::FloatRect worldBounds) {
    checkNodeCollision(sceneGraph, collisionPairs, worldBounds);
    
    for(Ptr& child : sceneGraph.mChildren)
        checkSceneCollision(*child, collisionPairs, worldBounds);
}

void SceneNode::checkNodeCollision(SceneNode &node, std::set<SceneNode::Pair> &collisionPairs, sf::FloatRect worldBounds) {
    if(this != &node && collision(*this, node) && !isDestroyed() && !node.isDestroyed()
            && this->getBoundingRect().intersects(worldBounds))
        collisionPairs.insert(std::minmax(this, &node));
    
    for(Ptr& child : mChildren)
        child->checkNodeCollision(node, collisionPairs, worldBounds);
}

bool SceneNode::isDestroyed() const {
    return false;
}

bool SceneNode::isMarkedForRemoval() const {
    return isDestroyed();
}

void SceneNode::removeWrecks() {
    auto wreckfieldBegin = std::remove_if(mChildren.begin(), mChildren.end(), std::mem_fn(&SceneNode::isMarkedForRemoval));
    mChildren.erase(wreckfieldBegin, mChildren.end());
    
    std::for_each(mChildren.begin(), mChildren.end(), std::mem_fn(&SceneNode::removeWrecks));
}

void SceneNode::sortChildren() {
    std::sort(mChildren.begin(), mChildren.end(), [] (Ptr& lhs, Ptr& rhs){
        return lhs->getPosition().y < rhs->getPosition().y;
    });
}

float distance(const SceneNode& lhs, const SceneNode& rhs) {
    return length(lhs.getWorldPosition() - rhs.getWorldPosition());
}

bool collision(const SceneNode& lhs, const SceneNode& rhs){
    return lhs.getBoundingRect().intersects(rhs.getBoundingRect());
}
