#include <iostream>
#include "../Include/Application.hpp"
#include <windows.h>

int main() {
//    FreeConsole();
    try{
        Application app;
        app.run();
    }
    catch(std::exception& e){
        std::cout << "\nEXCEPTION: " << e.what() << std::endl;
    }
}