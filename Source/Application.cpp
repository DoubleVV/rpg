//
// Created by DoubleVV on 01/08/2016.
//

#include "../Include/Application.hpp"
#include "../Include/State.hpp"
#include "../Include/StateIdentifiers.hpp"
#include "../Include/GameState.hpp"
#include "../Include/Utility.hpp"

const sf::Time Application::TimePerFrame = sf::seconds(1.f/60.f);

Application::Application()
        : mWindow(sf::VideoMode(1280,720), "Game"),
          mTextures(),
          mFonts(),
          mPlayer(),
          mStateStack(State::Context(mWindow, mTextures, mFonts, mPlayer)),
          mStatisticsText(),
          mStatisticsUpdateTime(),
          mStatisticsNumFrames(0) {
    mWindow.setKeyRepeatEnabled(false);

    mFonts.load(Fonts::Stats, "Media/Font/Sansation.ttf");
    mFonts.load(Fonts::Main, "Media/Font/Manaspc.ttf");

    mStatisticsText.setFont(mFonts.get(Fonts::Stats));
    mStatisticsText.setPosition(5.f, 5.f);
    mStatisticsText.setCharacterSize(10u);

    registerStates();
    mStateStack.pushState(States::Game);
}

void Application::registerStates() {
//    mStateStack.registerState<TitleState>(States::Title);
//    mStateStack.registerState<MenuState>(States::Menu);
    mStateStack.registerState<GameState>(States::Game);
//    mStateStack.registerState<PauseState>(States::Pause);
//    mStateStack.registerState<LoadingState>(States::Loading);
//    mStateStack.registerState<SettingsState>(States::Settings);
//    mStateStack.registerState<GameOverState>(States::GameOver);
}

void Application::processInput() {
    sf::Event event;
    
    while(mWindow.pollEvent(event)){
        mStateStack.handleEvent(event);
        
        if(event.type == sf::Event::Closed)
            mWindow.close();
    }
}

void Application::update(sf::Time dt) {
    mStateStack.update(dt);
}

void Application::render() {
    mWindow.clear();
    mStateStack.draw();
    
    mWindow.setView(mWindow.getDefaultView());
    mWindow.draw(mStatisticsText);

    mWindow.display();
}

void Application::run() {
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    while(mWindow.isOpen()){
        sf::Time dt = clock.restart();
        timeSinceLastUpdate += dt;
         while(timeSinceLastUpdate > TimePerFrame){
            timeSinceLastUpdate -= TimePerFrame;
            processInput();
            update(TimePerFrame);
            
            if(mStateStack.isEmpty())
                mWindow.close();
        }
        updateStatistics(dt);
        render();
    }
}

void Application::updateStatistics(sf::Time dt)
{
    mStatisticsUpdateTime += dt;
    mStatisticsNumFrames += 1;
    if (mStatisticsUpdateTime >= sf::seconds(1.0f))
    {
        mStatisticsText.setString("FPS: " + toString(mStatisticsNumFrames));

        mStatisticsUpdateTime -= sf::seconds(1.0f);
        mStatisticsNumFrames = 0;
    }
}
