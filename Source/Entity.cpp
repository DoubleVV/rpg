//
// Created by DoubleVV on 27/07/2016.
//

#include <c++/cassert>
#include "../Include/Entity.hpp"

Entity::Entity(int maxHealth)
        : mVelocity(),
          mMaxHealth(maxHealth),
          mHealth(maxHealth){
    
}

void Entity::setVelocity(sf::Vector2f velocity) {
    mVelocity = velocity;
}

void Entity::setVelocity(float vx, float vy) {
    mVelocity.x = vx;
    mVelocity.y = vy;
}

sf::Vector2f Entity::getVelocity() const {
    return mVelocity;
}

void Entity::updateCurrent(sf::Time dt, CommandQueue& commands) {
    move(mVelocity * dt.asSeconds());
}

void Entity::repair(int points) {
    assert(points > 0);
    
    mHealth = std::min(mHealth + points, mMaxHealth);
}

void Entity::damage(int points) {
    assert(points > 0);
    
    mHealth -= points;
}

void Entity::destroy() {
    mHealth = 0;
}

int Entity::getHealth() const {
    return mHealth;
}

int Entity::getMaxHealth() const {
    return mMaxHealth;
}

bool Entity::isDestroyed() const {
    return mHealth <= 0;
}

void Entity::accelerate(sf::Vector2f velocity) {
    mVelocity += velocity;
}

void Entity::accelerate(float vx, float vy) {
    sf::Vector2f velocity(vx, vy);
    setVelocity(getVelocity() + velocity);
}


