//
// Created by Walid on 10/10/2016.
//

#include "../Include/Character.hpp"
#include "../Include/ResourceHolder.hpp"
#include "../Include/Utility.hpp"
#include "../Include/CollidableNode.hpp"

Character::Character(Name name, const TextureHolder &textures, const FontHolder &fonts)
        : Entity(100),
          mName(name),
          mIsAttacking(false),
          mAttackCountdown(sf::Time::Zero),
          mHitCoundown(sf::Time::Zero),
          mDirectionIndex(Down),
          mSprite(sf::seconds(0.1f)),
          mAttackCommand() {
    initializeAnimation(textures);
    centerOrigin(mSprite);
    
    mAttackCommand.category = Category::SceneSpriteLayer;
    mAttackCommand.action = [this, &textures] (SceneNode& node, sf::Time)
    {
        createAttack(node, 0.f, 0.f, textures);
    };
}

void Character::drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const {
    target.draw(mSprite, states);
}

void Character::updateCurrent(sf::Time dt, CommandQueue &commands) {
    
    if(mAttackCountdown <= sf::Time::Zero)
        Entity::updateCurrent(dt, commands);
    updateAnimation(dt, commands);
    checkAttack(dt, commands);
}

void Character::updateAnimation(sf::Time dt, CommandQueue &commands) {
    
    if(mSprite.isLooped() || !mSprite.isPlaying()){
        if(getVelocity().x > 0){
            currentAnimation = &mWalkingAnimation[Right];
            mDirectionIndex = Right;
        }
        else if(getVelocity().y > 0){
            currentAnimation = &mWalkingAnimation[Down];
            mDirectionIndex = Down;
        }
        else if(getVelocity().x < 0){
            currentAnimation = &mWalkingAnimation[Left];
            mDirectionIndex = Left;
        }
        else if(getVelocity().y < 0){
            currentAnimation = &mWalkingAnimation[Up];
            mDirectionIndex = Up;
        }
        else
            currentAnimation = &mStandingAnimation[mDirectionIndex];

        mSprite.setLooped(true);
    
    
        if(mIsAttacking){
            currentAnimation = &mAttackAnimation[mDirectionIndex];
            mSprite.setLooped(false);
        }
    
        mSprite.play(*currentAnimation);
    }
    
    mSprite.update(dt);

}

unsigned int Character::getCategory() const {
    switch(mName){
        case MainTest :
            return Category::PlayerCharacter;
        case NpcTest :
            return Category::NonPlayableCharacter;
        case MonsterTest :
            return Category::EnemyCharacter;
        default :
            return Entity::getCategory();
    }
}

void Character::initializeAnimation(const TextureHolder &texture) {
    mWalkingAnimation[Down].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mWalkingAnimation[Down].addFrame(sf::IntRect(32, 0, 32, 32));
    mWalkingAnimation[Down].addFrame(sf::IntRect(64, 0, 32, 32));
    mWalkingAnimation[Down].addFrame(sf::IntRect(32, 0, 32, 32));
    mWalkingAnimation[Down].addFrame(sf::IntRect( 0, 0, 32, 32));
    
    mWalkingAnimation[Left].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mWalkingAnimation[Left].addFrame(sf::IntRect(32, 32, 32, 32));
    mWalkingAnimation[Left].addFrame(sf::IntRect(64, 32, 32, 32));
    mWalkingAnimation[Left].addFrame(sf::IntRect(32, 32, 32, 32));
    mWalkingAnimation[Left].addFrame(sf::IntRect( 0, 32, 32, 32));
    
    mWalkingAnimation[Right].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mWalkingAnimation[Right].addFrame(sf::IntRect(32, 64, 32, 32));
    mWalkingAnimation[Right].addFrame(sf::IntRect(64, 64, 32, 32));
    mWalkingAnimation[Right].addFrame(sf::IntRect(32, 64, 32, 32));
    mWalkingAnimation[Right].addFrame(sf::IntRect( 0, 64, 32, 32));
    
    mWalkingAnimation[Up].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mWalkingAnimation[Up].addFrame(sf::IntRect(32, 96, 32, 32));
    mWalkingAnimation[Up].addFrame(sf::IntRect(64, 96, 32, 32));
    mWalkingAnimation[Up].addFrame(sf::IntRect(32, 96, 32, 32));
    mWalkingAnimation[Up].addFrame(sf::IntRect( 0, 96, 32, 32));
    
    mAttackAnimation[Down].setSpriteSheet(texture.get(Textures::SpriteSheetTest));
    mAttackAnimation[Down].addFrame(sf::IntRect(49, 186, 36, 46));
    mAttackAnimation[Down].addFrame(sf::IntRect(88, 183, 32, 44));
    mAttackAnimation[Down].addFrame(sf::IntRect(120, 197, 21, 30));
    
    mAttackAnimation[Left].setSpriteSheet(texture.get(Textures::SpriteSheetTest));
    mAttackAnimation[Left].addFrame(sf::IntRect(120, 236, -45, 46));
    mAttackAnimation[Left].addFrame(sf::IntRect(165, 233, -45, 44));
    mAttackAnimation[Left].addFrame(sf::IntRect(210, 247, -45, 30));
    
    mAttackAnimation[Right].setSpriteSheet(texture.get(Textures::SpriteSheetTest));
    mAttackAnimation[Right].addFrame(sf::IntRect(120, 236, 45, 46));
    mAttackAnimation[Right].addFrame(sf::IntRect(165, 233, 45, 44));
    mAttackAnimation[Right].addFrame(sf::IntRect(210, 247, 45, 30));
    
    mAttackAnimation[Up].setSpriteSheet(texture.get(Textures::SpriteSheetTest));
    mAttackAnimation[Up].addFrame(sf::IntRect(49, 236, 36, 46));
    mAttackAnimation[Up].addFrame(sf::IntRect(88, 233, 32, 44));
    mAttackAnimation[Up].addFrame(sf::IntRect(120, 247, 21, 30));
    
    mStandingAnimation[Left].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mStandingAnimation[Left].addFrame(sf::IntRect(32, 32, 32, 32));
    
    mStandingAnimation[Down].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mStandingAnimation[Down].addFrame(sf::IntRect(32, 0, 32, 32));
    
    mStandingAnimation[Right].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mStandingAnimation[Right].addFrame(sf::IntRect(32, 64, 32, 32));
    
    mStandingAnimation[Up].setSpriteSheet(texture.get(Textures::SpriteSheet));
    mStandingAnimation[Up].addFrame(sf::IntRect(32, 96, 32, 32));
    
    currentAnimation = &mWalkingAnimation[mDirectionIndex];
    mSprite.play(*currentAnimation);
}

void Character::attack() {
    mIsAttacking = true;
}

void Character::checkAttack(sf::Time dt, CommandQueue &commands) {
    if(mIsAttacking && mAttackCountdown <= sf::Time::Zero){
        commands.push(mAttackCommand);
        mAttackCountdown +=  mSprite.getFrameTime().asSeconds() * sf::seconds((float)mAttackAnimation[mDirectionIndex].getSize());
        mIsAttacking = false;
    }
    else if(mAttackCountdown > sf::Time::Zero){
        mAttackCountdown -= dt;
        mIsAttacking = false;
    }
}

sf::FloatRect Character::getBoundingRect() const {
    return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

bool Character::isAllied() const{
    return (getCategory() != Category::EnemyCharacter);
}

void Character::createAttack(SceneNode &node, float xOffset, float yOffset, const TextureHolder &textures) {
    Category::Type category = Category::None;
    
    switch(getCategory()){
        case Category::PlayerCharacter :
            category = Category::PlayerHit;
            break;
        case Category::EnemyCharacter :
            category = Category::EnemyHit;
            break;
    }
    
    sf::FloatRect rect = getBoundingRect();
    rect.height *= 2;
    rect.width *= 2;
    
    sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
    
    std::unique_ptr<CollidableNode> collidableNode(new CollidableNode(category, rect));
//    collidableNode.destroy();
    
    node.attachChild(std::move(collidableNode));
}







