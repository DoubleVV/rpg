//
// Created by DoubleVV on 28/07/2016.
//

#include <algorithm>
#include <cmath>
#include <limits>
#include <fstream>

#include <SFML/Graphics/RenderWindow.hpp>

#include "../Include/World.hpp"
#include "../Include/TextNode.hpp"
#include "../Include/Map.hpp"
#include "../Include/Character.hpp"
#include "../Include/Utility.hpp"
#include "../Include/DataTables.hpp"

namespace{
    const std::vector<MapData> Table = initializeMapData();
}

World::World(sf::RenderWindow &window, FontHolder& fonts)
        : mWindow(window),
          mFonts(fonts),
//          mWorldView(sf::FloatRect(0.f, 0.f, 352, 240)),
          mWorldView(sf::FloatRect(0.f, 0.f, 426, 240)),
          mTextures(),
          mSceneGraph(),
          mSceneLayers(),
          mWorldBounds(0.f,0.f,mWorldView.getSize().x,2000.f),
          mPlayerCharacter(nullptr),
//          mMap(nullptr),
          mSpawnPosition(32, 32) {

    loadTextures();
    buildScene();

    mWorldView.setCenter(mWorldBounds.left, mWorldBounds.top);

    addNpcs();
}

void World::loadTextures() {
    mTextures.load(Textures::TileSet, "Media/Textures/Tiles.png");
    mTextures.load(Textures::SpriteSheetTest, "Media/Textures/HawkeyeNinja.png");
    mTextures.load(Textures::SpriteSheet, "Media/Textures/SpriteSheet.png");
}

void World::buildScene() {
    for(std::size_t i = 0; i < LayerCount; ++i) {
        Category::Type category;

        switch(i){
            case Background :
                category = Category::SceneBackgroundLayer;
                break;
            case Ground :
                category = Category::SceneGroundLayer;
                break;
            case Sprite :
                category = Category::SceneSpriteLayer;
                break;
            case Air :
                category = Category::SceneAirLayer;
                break;
            default :
                category = Category::None;
        }

        SceneNode::Ptr layer(new SceneNode(category));
        mSceneLayers[i] = layer.get();

        mSceneGraph.attachChild(std::move(layer));
    }

    //Setting up the Map
    //Building the Map
    buildMap(Maps::Test);
//    mMap->setPosition(mWorldBounds.left, mWorldBounds.top);
//    mSceneLayers[Background]->attachChild(std::move(map));

    //Setting the World Bounds to the correct size
//    mWorldBounds = sf::FloatRect(0.f, 0.f, mMap->getWidth()*16, mMap->getHeight()*16);

    //Setting up the main Character
    std::unique_ptr<Character> character(new Character(Character::MainTest, mTextures, mFonts));
    mPlayerCharacter = character.get();

    mPlayerCharacter->setPosition(mSpawnPosition);
    mSceneLayers[Sprite]->attachChild(std::move(character));
}

void World::update(sf::Time dt) {
    mPlayerCharacter->setVelocity(0.f, 0.f);

    while (!mCommandQueue.isEmpty())
        mSceneGraph.onCommand(mCommandQueue.pop(), dt);
    
    handleCollisions();
    
    mSceneGraph.removeWrecks();

    spawnNpcs();

    mSceneLayers[Sprite]->sortChildren();
    
    updateMap(dt);
    mSceneGraph.update(dt, mCommandQueue);
    adaptViewPosition();
}

void World::draw() {
    mWindow.setView(mWorldView);
    mWindow.draw(mSceneGraph);
}

CommandQueue &World::getCommandQueue() {
    return mCommandQueue;
}

void World::adaptPlayerVelocity() {
}

void World::adaptPlayerPosition() {
}

sf::FloatRect World::getBattlefieldBounds() const {
    sf::FloatRect bounds = getViewBounds();
//    bounds.top -= 100.f;
//    bounds.height += 100.f;

    return bounds;
}

sf::FloatRect World::getViewBounds() const {
    return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize()/2.f, mWorldView.getSize());
}

void World::handleCollisions() {
    std::set<SceneNode::Pair> collisionPairs;
    mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs, getBattlefieldBounds());

    for(SceneNode::Pair pair : collisionPairs){
        if(matchesCategories(pair, Category::PlayerCharacter, Category::NonPlayableCharacter)){
            auto& player = static_cast<Character&> (*pair.first);
            auto& npc = static_cast<Character&> (*pair.second);
    
            sf::Vector2f pushDirection = unitVector(player.getWorldPosition() - npc.getWorldPosition());
    
            sf::Vector2f push = pushDirection * 2.f;
            player.move(push);
        }
        else if(matchesCategories(pair, Category::PlayerCharacter, Category::MapWall)){
            auto& player = static_cast<Character&> (*pair.first);
            auto& wall = static_cast<CollidableNode&> (*pair.second);
    
            sf::Vector2f pushDirection = unitVector(player.getWorldPosition() - wall.getWorldPosition());
    
            sf::Vector2f push = pushDirection * 2.f;
            player.move(push);
        }
    }
}


bool World::hasAlivePlayer() const {
    return true;
}

void World::adaptViewPosition() {
    sf::Vector2f viewBoundsSize(mWorldView.getSize());
    const float borderDistance = 100.f;

    sf::Vector2f center = mWorldView.getCenter();

    sf::Vector2f position = mPlayerCharacter->getPosition();
    center.x = std::max(center.x, position.x - (viewBoundsSize.x/2.f - borderDistance));
    center.x = std::min(center.x, position.x + (viewBoundsSize.x/2.f - borderDistance));

    center.y = std::max(center.y, position.y - (viewBoundsSize.y/2.f - borderDistance));
    center.y = std::min(center.y, position.y + (viewBoundsSize.y/2.f - borderDistance));

    center.x = std::ceil(center.x);
    center.y = std::ceil(center.y);

    mWorldView.setCenter(center);

//    std::cout << center.x << " " << center.y << std::endl;
}

void World::spawnNpcs() {
    while(!mNpcSpawnPoints.empty()){
        SpawnPoint spawn = mNpcSpawnPoints.back();

        std::unique_ptr<Character> npc(new Character(spawn.name, mTextures, mFonts));
        npc->setPosition(spawn.x, spawn.y);

        mSceneLayers[Sprite]->attachChild(std::move(npc));

        mNpcSpawnPoints.pop_back();
    }
}

void World::addNpc(Character::Name type, float relX, float relY) {
    SpawnPoint spawn(type, relX, relY);
    mNpcSpawnPoints.push_back(spawn);
}

void World::addNpcs() {
    addNpc(Character::NpcTest, 200.f, 128.f);
}

int World::getPlayerCharacterHealth() const {
    return mPlayerCharacter->getHealth();
}

int World::getPlayerCharacterMaxHealth() const {
    return mPlayerCharacter->getMaxHealth();
}

sf::Vector2f World::getPlayerCharacterPosition() const {
    return mPlayerCharacter->getPosition();
}

void World::buildMap(Maps::ID mapName){
    std::ifstream mapFileStream(Table[mapName].path);
    
    mapFileStream >> mMapData;
    
    int width = mMapData["width"];
    int height = mMapData["height"];
    
    for(std::size_t l = 0; l < LayerCount; ++l){
        if(mMapData["layers"][l].empty())
            break;
        
        std::unique_ptr<VertexArrayNode> vertexArray(new VertexArrayNode(mTextures));
        
        vertexArray->getArray().resize((size_t)width * height * 4);
        
        for (unsigned int i = 0; i < width; ++i)
            for (unsigned int j = 0; j < height; ++j)
            {
                // on récupère le numéro de tuile courant
                int tileNumber = mMapData["layers"][l]["data"][i + j * width];
                
                int tu = mMapData["tilesets"][0]["imagewidth"];
                int tv = mMapData["tilesets"][0]["imageheight"];
                
                if(tileNumber > 0){
                    tileNumber -= 1;
                    
                    // on en déduit sa position dans la texture du tileset
                    tu = tileNumber % ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
                    tv = tileNumber / ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
    
                    // on récupère un pointeur vers le quad à définir dans le tableau de vertex
                    sf::Vertex* quad = &vertexArray->getArray()[(i + j * width) * 4];
    
                    // on définit ses quatre coins
                    quad[0].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[1].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[2].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[3].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
    
                    // on définit ses quatre coordonnées de texture
                    quad[0].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[1].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[2].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                    quad[3].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
    
                    // on cherche si le tile est animé
                    for(auto animation : mMapData["tilesets"][0]["tiles"]){
                        if (animation["animation"][0]["tileid"] == tileNumber){
                            AnimatedTile animatedTile;
            
                            animatedTile.sCurrentTime = sf::Time::Zero;
                            animatedTile.sCurrentTile = 0;
                            animatedTile.sVertex = &vertexArray->getArray()[(i + j * width) * 4];
            
                            // on ajoute chaque frame de l'animation
                            for(auto frames : animation["animation"]){
                                animatedTile.sFrameTime[frames["tileid"]] = sf::milliseconds(frames["duration"]);
                                animatedTile.sFrames.push_back(frames["tileid"]);
                            }
            
                            mAnimatedTiles.push_back(std::move(animatedTile));
                            break;
                        }
                    }
    
                    // on cherche si le tile est plein (si c'est un mur etc...)
                    for(auto collidable : mMapData["tilesets"][0]["tileproperties"]){
                        if(!mMapData["tilesets"][0]["tileproperties"][std::to_string(tileNumber)].empty()){
                            std::unique_ptr<CollidableNode> collidableTile(new CollidableNode(Category::MapWall, quad));
            
                            mSceneLayers[Air]->attachChild(std::move(collidableTile));
                            break;
                        }
                    }
                }
            }
        mSceneLayers[l]->attachChild(std::move(vertexArray));
    }
}

void World::updateMap(sf::Time dt){
    // pour chaque tiles animés on met a jour leur temps
    for(auto& tile : mAnimatedTiles){
        tile.sCurrentTime += dt;
        
        if(tile.sCurrentTime >= tile.sFrameTime[tile.sFrames[tile.sCurrentTile]]){
            tile.sCurrentTile = (tile.sCurrentTile+1) % tile.sFrames.size();
            
            int tileNumber = tile.sFrames[tile.sCurrentTile];
            
            // on en déduit sa position dans la texture du tileset
            int tu = tileNumber % ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
            int tv = tileNumber / ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
            
            // on définit ses quatre coordonnées de texture
            tile.sVertex[0].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[1].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[2].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[3].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
            
            tile.sCurrentTime = sf::Time::Zero;
        }
    }
}


bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2){
    unsigned int category1 = colliders.first->getCategory();
    unsigned int category2 = colliders.second->getCategory();
    
    if(type1 & category1 && type2 & category2)
        return true;
    else if (type1 & category2 && type2 & category1){
        std::swap(colliders.first, colliders.second);
        return true;
    }
    else
        return false;
}