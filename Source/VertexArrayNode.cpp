//
// Created by DoubleVV on 23/01/2017.
//

#include <SFML/Graphics/PrimitiveType.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "../Include/VertexArrayNode.hpp"
#include "../Include/ResourceHolder.hpp"
#include "../Include/DataTables.hpp"

namespace{
    const std::vector<MapData> Table = initializeMapData();
}

VertexArrayNode::VertexArrayNode(TextureHolder& textures)
        : mTexture(textures),
          mType() {
    mArray.setPrimitiveType(sf::Quads);
}

void VertexArrayNode::drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const {
    states.texture = &mTexture.get(Table[mType].texture);
    target.draw(mArray, states);
}

unsigned int VertexArrayNode::getCategory() const {
    switch(mType){
        case Background :
            return Category::SceneBackgroundLayer;
        case Ground :
            return Category::SceneGroundLayer;
        case Sprite :
            return Category::SceneSpriteLayer;
        case Air :
            return Category::SceneAirLayer;
        default:
            return Category::None;
    }
}

void VertexArrayNode::setType(VertexArrayNode::Type type) {
    mType = type;
}

sf::VertexArray& VertexArrayNode::getArray() {
    return mArray;
}
