//
// Created by DoubleVV on 31/07/2016.
//

#include <string>
#include <algorithm>
#include <map>

#include "../Include/Player.hpp"
#include "../Include/CommandQueue.hpp"
#include "../Include/Character.hpp"

using namespace std::placeholders;

struct CharacterMover{
    CharacterMover(float vx, float vy)
            : velocity(vx, vy){

    }

    void operator()(Character& character, sf::Time) const{
        character.accelerate(velocity.x, velocity.y);
    }

    sf::Vector2f velocity;
};

Player::Player()
        : mCurrentMissionStatus(MissionRunning) {
    mKeyBinding[sf::Keyboard::Left] = MoveLeft;
    mKeyBinding[sf::Keyboard::Right] = MoveRight;
    mKeyBinding[sf::Keyboard::Up] = MoveUp;
    mKeyBinding[sf::Keyboard::Down] = MoveDown;
    mKeyBinding[sf::Keyboard::Space] = Attack;
    mKeyBinding[sf::Keyboard::M] = LaunchMissile;
    
    initializeActions();
    
    for(auto& pair : mActionBinding)
        pair.second.category = Category::PlayerCharacter;
}

void Player::handleRealTimeInput(CommandQueue &commands) {
    for(auto pair : mKeyBinding){
        if(sf::Keyboard::isKeyPressed(pair.first) && isRealTimeAction(pair.second))
            commands.push(mActionBinding[pair.second]);
    }
}

void Player::handleEvent(const sf::Event &event, CommandQueue &commands) {
    if (event.type == sf::Event::KeyPressed)
    {
        // Check if pressed key appears in key binding, trigger command if so
        auto found = mKeyBinding.find(event.key.code);
        if (found != mKeyBinding.end() && !isRealTimeAction(found->second))
            commands.push(mActionBinding[found->second]);
    }
}

void Player::initializeActions() {
    const float playerSpeed = 100.f;

    mActionBinding[MoveLeft].action = derivedAction<Character>(CharacterMover(-playerSpeed, 0.f));
    mActionBinding[MoveRight].action = derivedAction<Character>(CharacterMover(playerSpeed, 0.f));
    mActionBinding[MoveUp].action = derivedAction<Character>(CharacterMover(0.f, -playerSpeed));
    mActionBinding[MoveDown].action = derivedAction<Character>(CharacterMover(0.f, playerSpeed));
    
    mActionBinding[Attack].action = derivedAction<Character>(std::bind(&Character::attack, _1));



//    mActionBinding[MoveLeft].action = derivedAction<Aircraft>(AircraftMover(-playerSpeed, 0.f));
//
//    mActionBinding[Fire].action = derivedAction<Aircraft>(std::bind(&Aircraft::fire, _1));
}

bool Player::isRealTimeAction(Player::Action action) {
    switch(action){
        case MoveLeft:
        case MoveRight:
        case MoveDown:
        case MoveUp:
            return true;
    
        default:
            return false;
    }
}

void Player::assignKey(Player::Action action, sf::Keyboard::Key key) {
    for(auto itr = mKeyBinding.begin(); itr != mKeyBinding.end(); ){
        if(itr->second == action)
            mKeyBinding.erase(itr++);
        else
            ++itr;
    }
    
    mKeyBinding[key] = action;
}

sf::Keyboard::Key Player::getAssignedKey(Player::Action action) const {
    for(auto pair : mKeyBinding){
        if(pair.second == action)
            return pair.first;
    }
    
    return sf::Keyboard::Unknown;
}

void Player::setMissionStatus(Player::MissionStatus status) {
    mCurrentMissionStatus = status;
}

Player::MissionStatus Player::getMissionStatus() const {
    return mCurrentMissionStatus;
}

