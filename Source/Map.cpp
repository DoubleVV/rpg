//
// Created by DoubleVV on 28/09/2016.
//

#include <SFML/Graphics/RenderTarget.hpp>

#include <fstream>
#include <iostream>

#include "../Include/Map.hpp"
#include "../Include/ResourceHolder.hpp"
#include "../Include/DataTables.hpp"
#include "../Include/World.hpp"

namespace{
    const std::vector<MapData> Table = initializeMapData();
}

Map::Map(TextureHolder &textures, FontHolder &fonts)
        : mTexture(textures),
          mFont(fonts),
          mType(Maps::Test),
          mWidth(0),
          mHeight(0) {

    initializeMapLayers();
}

void Map::drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const {
    states.texture = &mTexture.get(Table[mType].texture);
}

void Map::updateCurrent(sf::Time dt, CommandQueue &commands) {

    // pour chaque tiles animés on met a jour leur temps
    for(auto& tile : mAnimatedTiles){
        tile.sCurrentTime += dt;

        if(tile.sCurrentTime >= tile.sFrameTime[tile.sFrames[tile.sCurrentTile]]){
            tile.sCurrentTile = (tile.sCurrentTile+1) % tile.sFrames.size();

            int tileNumber = tile.sFrames[tile.sCurrentTile];

            // on en déduit sa position dans la texture du tileset
            int tu = tileNumber % ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
            int tv = tileNumber / ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);

            // on définit ses quatre coordonnées de texture
            tile.sVertex[0].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[1].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[2].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
            tile.sVertex[3].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);

            tile.sCurrentTime = sf::Time::Zero;
        }
    }
}

void Map::clearMap() {
//    mTile.clear();
}

void Map::buildMap(Maps::ID mapName) {
    mType = mapName;
    std::ifstream mapFileStream(Table[mType].path);

    mapFileStream >> mMapData;

    mWidth = mMapData["width"];
    mHeight = mMapData["height"];

    int l = 0;
    
    for(size_t i = 0; i < World::Layer::LayerCount; ++i){
        if(mMapData["layers"][l].empty())
            break;

        std::unique_ptr<VertexArrayNode> vertexArray(new VertexArrayNode(mTexture));
    
        switch(i){
            case World::Layer::Background :
                vertexArray->setType(VertexArrayNode::Type::Background);
                break;
            case World::Layer::Ground :
                vertexArray->setType(VertexArrayNode::Type::Ground);
                break;
            case World::Layer::Sprite :
                vertexArray->setType(VertexArrayNode::Type::Sprite);
                break;
            case World::Layer::Air :
                vertexArray->setType(VertexArrayNode::Type::Air);
                break;
            default:
                break;
        }
        
        vertexArray->getArray().resize((size_t)mWidth * mHeight * 4);
    
        for (unsigned int i = 0; i < mWidth; ++i)
            for (unsigned int j = 0; j < mHeight; ++j)
            {
                // on récupère le numéro de tuile courant
                int tileNumber = mMapData["layers"][l]["data"][i + j * mWidth];
            
                int tu = mMapData["tilesets"][0]["imagewidth"];
                int tv = mMapData["tilesets"][0]["imageheight"];
            
                if(tileNumber > 0){
                    tileNumber -= 1;
                
                    // on en déduit sa position dans la texture du tileset
                    tu = tileNumber % ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
                    tv = tileNumber / ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
                }
            
                // on cherche si le tile est animé
                for(auto animation : mMapData["tilesets"][0]["tiles"]){
                    if (animation["animation"][0]["tileid"] == tileNumber){
                        AnimatedTile animatedTile;
                    
                        animatedTile.sCurrentTime = sf::Time::Zero;
                        animatedTile.sCurrentTile = 0;
                        animatedTile.sVertex = &vertexArray->getArray()[(i + j * mWidth) * 4];
                    
                        // on ajoute chaque frame de l'animation
                        for(auto frames : animation["animation"]){
                            animatedTile.sFrameTime[frames["tileid"]] = sf::milliseconds(frames["duration"]);
                            animatedTile.sFrames.push_back(frames["tileid"]);
                        }
                    
                        mAnimatedTiles.push_back(std::move(animatedTile));
                        break;
                    }
                }
            
                // on récupère un pointeur vers le quad à définir dans le tableau de vertex
                sf::Vertex* quad = &vertexArray->getArray()[(i + j * mWidth) * 4];
            
                // on définit ses quatre coins
                quad[0].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[1].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[2].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[3].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
            
                // on définit ses quatre coordonnées de texture
                quad[0].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[1].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[2].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[3].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
            
                // on cherche si le tile est plein (si c'est un mur etc...)
                for(auto collidable : mMapData["tilesets"][0]["tileproperties"]){
                    if(!mMapData["tilesets"][0]["tileproperties"][std::to_string(tileNumber)].empty()){
                        std::unique_ptr<CollidableNode> collidableTile(new CollidableNode(Category::MapWall, quad));
                    
                        attachChild(std::move(collidableTile));
                        break;
                    }
                }
            }
        l++;
//        attachChild(std::move(vertexArray));
    }
    
    
    /*for(auto &layer : mMapData["layers"]){
        mLayers[l].resize((size_t)mWidth * mHeight * 4);

        for (unsigned int i = 0; i < mWidth; ++i)
            for (unsigned int j = 0; j < mHeight; ++j)
            {
                // on récupère le numéro de tuile courant
                int tileNumber = layer["data"][i + j * mWidth];

                int tu = mMapData["tilesets"][0]["imagewidth"];
                int tv = mMapData["tilesets"][0]["imageheight"];

                if(tileNumber > 0){
                    tileNumber -= 1;

                    // on en déduit sa position dans la texture du tileset
                    tu = tileNumber % ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
                    tv = tileNumber / ((int)mMapData["tilesets"][0]["imagewidth"] / (int)mMapData["tilesets"][0]["tilewidth"]);
                }

                // on cherche si le tile est animé
                for(auto animation : mMapData["tilesets"][0]["tiles"]){
                    if (animation["animation"][0]["tileid"] == tileNumber){
                        AnimatedTile animatedTile;

                        animatedTile.sCurrentTime = sf::Time::Zero;
                        animatedTile.sCurrentTile = 0;
                        animatedTile.sVertex = &mLayers[l][(i + j * mWidth) * 4];

                        // on ajoute chaque frame de l'animation
                        for(auto frames : animation["animation"]){
                            animatedTile.sFrameTime[frames["tileid"]] = sf::milliseconds(frames["duration"]);
                            animatedTile.sFrames.push_back(frames["tileid"]);
                        }

                        mAnimatedTiles.push_back(std::move(animatedTile));
                        break;
                    }
                }

                // on récupère un pointeur vers le quad à définir dans le tableau de vertex
                sf::Vertex* quad = &mLayers[l][(i + j * mWidth) * 4];

                // on définit ses quatre coins
                quad[0].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[1].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], j * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[2].position = sf::Vector2f((i + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[3].position = sf::Vector2f(i * (int)mMapData["tilesets"][0]["tilewidth"], (j + 1) * (int)mMapData["tilesets"][0]["tileheight"]);

                // on définit ses quatre coordonnées de texture
                quad[0].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[1].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], tv * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[2].texCoords = sf::Vector2f((tu + 1) * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
                quad[3].texCoords = sf::Vector2f(tu * (int)mMapData["tilesets"][0]["tilewidth"], (tv + 1) * (int)mMapData["tilesets"][0]["tileheight"]);
    
                // on cherche si le tile est plein (si c'est un mur etc...)
                for(auto collidable : mMapData["tilesets"][0]["tileproperties"]){
                    if(!mMapData["tilesets"][0]["tileproperties"][std::to_string(tileNumber)].empty()){
                        std::unique_ptr<CollidableNode> collidableTile(new CollidableNode(quad));
            
                        attachChild(std::move(collidableTile));
                        break;
                    }
                }
            }

        l++;
    }*/
}

int Map::getWidth() const {
    return mWidth;
}

int Map::getHeight() const {
    return mHeight;
}

void Map::initializeMapLayers() {
//    for(size_t i = 0; i < World::Layer::LayerCount; ++i){
//
//        std::unique_ptr<VertexArrayNode> vertexArray(new VertexArrayNode(mTexture));
//
//        switch(i){
//            case World::Layer::Background :
//                vertexArray->setType(VertexArrayNode::Type::Background);
//                break;
//            case World::Layer::Ground :
//                vertexArray->setType(VertexArrayNode::Type::Ground);
//                break;
//            case World::Layer::Sprite :
//                vertexArray->setType(VertexArrayNode::Type::Sprite);
//                break;
//            case World::Layer::Air :
//                vertexArray->setType(VertexArrayNode::Type::Air);
//                break;
//            default:
//                break;
//        }
//        attachChild(std::move(vertexArray));
//    }
}

