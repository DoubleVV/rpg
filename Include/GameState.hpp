//
// Created by DoubleVV on 26/07/2016.
//

#ifndef SHOOTER_GAME_HPP
#define SHOOTER_GAME_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "World.hpp"
#include "Player.hpp"
#include "State.hpp"
#include "TextNode.hpp"

class GameState : public State {
    
public:
    
    GameState(StateStack& stack, Context context);
    
    virtual bool handleEvent(const sf::Event& event);
    virtual bool update(sf::Time deltaTime);
    virtual void draw();

    void updateHud();
    
private:
    
    World mWorld;
    Player& mPlayer;

    sf::Text mHealthDisplay;
    sf::Text mDebugPosition;
};


#endif //SHOOTER_GAME_HPP
