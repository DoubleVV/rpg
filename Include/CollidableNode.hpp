//
// Created by DoubleVV on 18/01/2017.
//

#ifndef GAME_COLLIDABLENODE_HPP
#define GAME_COLLIDABLENODE_HPP

#include <SFML/Graphics/VertexArray.hpp>

#include "SceneNode.hpp"
#include "Category.hpp"

class CollidableNode : public SceneNode {
public:
    explicit CollidableNode(Category::Type category, const sf::Vertex* quad);
    explicit CollidableNode(Category::Type category, const sf::FloatRect rect);
    
private:
    virtual unsigned int getCategory() const;
    sf::FloatRect getBoundingRect() const;
    
private:
    sf::FloatRect mRect;
    Category::Type mCategory;
};


#endif //GAME_COLLIDABLENODE_HPP
