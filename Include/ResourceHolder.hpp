//
// Created by DoubleVV on 26/07/2016.
//

#ifndef SHOOTER_RESOURCEHOLDER_HPP
#define SHOOTER_RESOURCEHOLDER_HPP


#include <c++/memory>
#include <c++/map>
#include <c++/cassert>

template <typename Resource, typename Identifier>
class ResourceHolder {
    
private:
    
    std::map<Identifier, std::unique_ptr<Resource>> mResourceMap;
    
public:
    
    void load(Identifier id, const std::string& filename);
    
    template <typename Parameter>
    void load(Identifier id, const std::string& filename, const Parameter& secondParam);
    
    Resource& get(Identifier id);
    const Resource& get(Identifier id) const;
    
};

template <typename Resource, typename Identifier>
void ResourceHolder<Resource, Identifier>::load(Identifier id, const std::string &filename) {
    std::unique_ptr<Resource> resource(new Resource());
    if(!resource->loadFromFile(filename))
        throw std::runtime_error("Resource::load - Failed to load " + filename);
    
    auto inserted = mResourceMap.insert(std::make_pair(id, std::move(resource)));
    assert(inserted.second);
}

template <typename Resource, typename Identifier>
template <typename Parameter>
void ResourceHolder<Resource, Identifier>::load(Identifier id, const std::string &filename, const Parameter &secondParam) {
    std::unique_ptr<Resource> resource(new Resource());
    if(!resource->loadFromFile(filename, secondParam))
        throw std::runtime_error("Resource::load - Failed to load " + filename);
    
    auto inserted = mResourceMap.insert(std::make_pair(id, std::move(resource)));
    assert(inserted.second);
}

template <typename Resource, typename Identifier>
Resource &ResourceHolder<Resource, Identifier>::get(Identifier id) {
    auto found = mResourceMap.find(id);
    assert(found != mResourceMap.end());
    
    return *found->second;
}

template <typename Resource, typename Identifier>
const Resource &ResourceHolder<Resource, Identifier>::get(Identifier id) const {
    auto found = mResourceMap.find(id);
    assert(found != mResourceMap.end());
    
    return *found->second;
}

#endif //SHOOTER_RESOURCEHOLDER_HPP
