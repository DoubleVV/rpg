//
// Created by DoubleVV on 31/07/2016.
//

#ifndef SHOOTER_PLAYER_HPP
#define SHOOTER_PLAYER_HPP


#include <SFML/Window/Event.hpp>
#include <c++/map>
#include "CommandQueue.hpp"

class Player {
public:
    enum Action{
        MoveLeft,
        MoveRight,
        MoveDown,
        MoveUp,
        Attack,
        LaunchMissile,
        ActionCount
    };
    
    enum MissionStatus{
        MissionRunning,
        MissionSuccess,
        MissionFailure
    };
    
public:
    Player();
    
    void handleEvent(const sf::Event& event, CommandQueue& commands);
    void handleRealTimeInput(CommandQueue& commands);
    
    void assignKey(Action action, sf::Keyboard::Key key);
    sf::Keyboard::Key getAssignedKey(Action action) const;
    
    void setMissionStatus(MissionStatus status);
    MissionStatus getMissionStatus() const;

private:
    static bool isRealTimeAction(Action action);
    void initializeActions();
    
private:
    std::map<sf::Keyboard::Key, Action> mKeyBinding;
    std::map<Action, Command> mActionBinding;
    MissionStatus mCurrentMissionStatus;
};


#endif //SHOOTER_PLAYER_HPP
