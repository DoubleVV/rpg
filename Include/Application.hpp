//
// Created by DoubleVV on 01/08/2016.
//

#ifndef SHOOTER_APPLICATION_HPP
#define SHOOTER_APPLICATION_HPP


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Text.hpp>

#include "StateStack.hpp"
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "Player.hpp"

class Application {
public:
    Application();
    void run();
    
private:
    void registerStates();
    void updateStatistics(sf::Time dt);
    
    void processInput();
    void update(sf::Time dt);
    void render();
    
private:
    static const sf::Time TimePerFrame;
    
    sf::RenderWindow mWindow;
    TextureHolder mTextures;
    FontHolder mFonts;
    Player mPlayer;
    
    StateStack mStateStack;

    sf::Text mStatisticsText;
    sf::Time mStatisticsUpdateTime;
    std::size_t mStatisticsNumFrames;
};


#endif //SHOOTER_APPLICATION_HPP
