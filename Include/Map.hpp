//
// Created by DoubleVV on 28/09/2016.
//

#ifndef GAME_MAP_HPP
#define GAME_MAP_HPP

#include <map>

#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "../Third parties/json/src/json.hpp"

#include "SceneNode.hpp"
#include "MapIndentifier.hpp"
#include "ResourceIdentifiers.hpp"
#include "CollidableNode.hpp"
#include "VertexArrayNode.hpp"

using json = nlohmann::json;

class Map : public SceneNode {
public:

    struct AnimatedTile{
        sf::Time sCurrentTime;
        int sCurrentTile;
        std::map<int, sf::Time> sFrameTime;
        std::vector<int> sFrames;
        sf::Vertex* sVertex;
    };

public:

    Map(TextureHolder& textures, FontHolder& fonts);

    void clearMap();
    void buildMap(Maps::ID mapName);

    int getWidth() const;
    int getHeight() const;

private:

    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

    void initializeMapLayers();

private:

//    std::vector<VertexArrayNode> mLayers;
    std::vector<AnimatedTile> mAnimatedTiles;

    Maps::ID mType;
    int mWidth;
    int mHeight;

    json mMapData;

    TextureHolder &mTexture;
    FontHolder &mFont;
};


#endif //GAME_MAP_HPP
