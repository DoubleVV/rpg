//
// Created by DoubleVV on 10/08/2016.
//

#ifndef SHOOTER_DATATABLES_HPP
#define SHOOTER_DATATABLES_HPP

#include <c++/vector>
#include <SFML/System/Time.hpp>
#include <c++/functional>
#include <SFML/Graphics/Rect.hpp>

#include "ResourceIdentifiers.hpp"
#include "MapIndentifier.hpp"

class Aircraft;

struct Direction{
    Direction(float angle, float distance)
            : angle(angle),
              distance(distance) {
        
    }
    
    float angle;
    float distance;
};

struct AircraftData{
    int hitpoints;
    float speed;
    Textures::ID texture;
    sf::IntRect textureRect;
    sf::Time fireInterval;
    std::vector<Direction> directions;
};

struct ProjectileData{
    int damage;
    float speed;
    Textures::ID texture;
    sf::IntRect textureRect;
};

struct PickupData{
    Textures::ID texture;
    sf::IntRect textureRect;
    std::function<void(Aircraft&)> action;
};

struct TileData{
    Textures::ID texture;
    sf::IntRect textureRect;
    bool isEmpty;
    unsigned int animationFrame;
};

struct MapData{
    std::string path;
    Textures::ID texture;
};

struct CharacterData{

};

std::vector<AircraftData> initializeAircraftData();
std::vector<ProjectileData> initializeProjectileData();
std::vector<PickupData> initializePickupData();
std::vector<TileData> initializeTileData();
std::vector<MapData> initializeMapData();

#endif //SHOOTER_DATATABLES_HPP
