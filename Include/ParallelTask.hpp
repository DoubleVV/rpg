//
// Created by DoubleVV on 04/08/2016.
//

#ifndef SHOOTER_PARALLELTASK_HPP
#define SHOOTER_PARALLELTASK_HPP


#include <SFML/System/Clock.hpp>
#include <SFML/System/Mutex.hpp>
#include <SFML/System/Lock.hpp>
#include <SFML/System/Thread.hpp>

class ParallelTask {
public:
    ParallelTask();
    void execute();
    bool isFinished();
    float getCompletion();

private:
    void runTask();
    
private:
    sf::Thread mThread;
    bool mFinished;
    sf::Clock mElapsedTime;
    sf::Mutex mMutex;
};


#endif //SHOOTER_PARALLELTASK_HPP
