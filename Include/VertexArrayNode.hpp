//
// Created by DoubleVV on 23/01/2017.
//

#ifndef GAME_VERTEXARRAYNODE_HPP
#define GAME_VERTEXARRAYNODE_HPP

#include <SFML/Graphics/VertexArray.hpp>

#include "SceneNode.hpp"
#include "ResourceIdentifiers.hpp"

class VertexArrayNode : public SceneNode {
public:
    enum Type{
        Background,
        Ground,
        Sprite,
        Air,
        LayerCount
    };
    
public:
    explicit VertexArrayNode(TextureHolder& textures);
    
    void setType(Type type);
    
    sf::VertexArray& getArray();
    virtual unsigned int getCategory() const;
    
private:
    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    
private:
    Type mType;
    sf::VertexArray mArray;
    TextureHolder& mTexture;
};


#endif //GAME_VERTEXARRAYNODE_HPP
