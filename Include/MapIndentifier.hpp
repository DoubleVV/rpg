//
// Created by DoubleVV on 28/09/2016.
//

#ifndef GAME_TILEIDENTIFIERS_HPP
#define GAME_TILEIDENTIFIERS_HPP

namespace Tiles{
    enum Type{
        Empty,
        Grass1,
        Grass2,
        Grass3,
        Grass4,
        GrassWater1,
        GrassWater2,
        GrassWater3,
        GrassWater4,
        WaterGrass1,
        WaterGrass2,
        WaterGrass3,
        WaterGrass4,
        Water1,
        Water2,
        Water3,
        Water4,
        Water5,
        GrassCliff1,
        GrassCliff2,
        Cliff1,
        Cliff2,
        Cliff3,
        Cliff4,
        TypeCount
    };
};

namespace Maps{
    enum ID{
        Village1,
        Village2,
        Forest1,
        Forest2,
        Castle1,
        Test,
        MapCount
    };
}

#endif //GAME_TILEIDENTIFIERS_HPP
