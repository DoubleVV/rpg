//
// Created by Walid on 10/10/2016.
//

#ifndef GAME_CHARACTER_HPP
#define GAME_CHARACTER_HPP


#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"
#include "AnimatedSprite.hpp"

class Character : public Entity {

public:
    
    enum Name{
        MainTest,
        NpcTest,
        MonsterTest,
        NameCount
    };
    
    enum Direction{
        Down,
        Left,
        Right,
        Up,
        DirectionCount
    };
    
public:
       
    Character(Name name, const TextureHolder &texture, const FontHolder &fonts);

    virtual unsigned int getCategory() const;
    sf::FloatRect getBoundingRect() const;
    bool isAllied() const;
    
    void attack();

private:
    AnimatedSprite mSprite;
    Name mName;
    Direction mDirectionIndex;
    
    std::array<Animation, DirectionCount> mWalkingAnimation;
    std::array<Animation, DirectionCount> mAttackAnimation;
    std::array<Animation, DirectionCount> mStandingAnimation;
    
    Animation* currentAnimation;
    
    bool mIsAttacking;
    sf::Time mAttackCountdown;
    sf::Time mHitCoundown; //TODO : nom temporaire
    
    Command mAttackCommand;

private:
    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
    
    void updateAnimation(sf::Time dt, CommandQueue& commands);
    
    void initializeAnimation(const TextureHolder &texture);
    
    void checkAttack(sf::Time dt, CommandQueue& commands);
    void createAttack(SceneNode& node, float xOffset, float yOffset, const TextureHolder& textures);
};


#endif //GAME_CHARACTER_HPP
