//
// Created by DoubleVV on 29/07/2016.
//

#ifndef SHOOTER_CATEGORY_HPP
#define SHOOTER_CATEGORY_HPP

namespace Category{
    enum Type{
        None = 0,
        SceneBackgroundLayer = 1 << 0,
        SceneGroundLayer = 1 << 1,
        SceneSpriteLayer = 1 << 2,
        SceneAirLayer = 1 << 3,
        PlayerCharacter = 1 << 4,
        NonPlayableCharacter = 1 << 5,
        EnemyCharacter = 1 << 6,
        MapWall = 1 << 7,
        PlayerHit = 1 << 8,
        EnemyHit = 1 << 9
    };
}

#endif //SHOOTER_CATEGORY_HPP
