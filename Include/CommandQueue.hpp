//
// Created by DoubleVV on 31/07/2016.
//

#ifndef SHOOTER_COMMANDQUEUE_HPP
#define SHOOTER_COMMANDQUEUE_HPP


#include <c++/queue>

#include "Command.hpp"

class CommandQueue {
public:
    void push(const Command& command);
    Command pop();
    bool isEmpty() const;
    
private:
    std::queue<Command> mQueue;
};


#endif //SHOOTER_COMMANDQUEUE_HPP
