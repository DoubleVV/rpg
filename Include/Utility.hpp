//
// Created by DoubleVV on 05/08/2016.
//

#ifndef SHOOTER_UTILITY_HPP
#define SHOOTER_UTILITY_HPP


#include <c++/string>
#include <SFML/Window/Keyboard.hpp>
#include <c++/sstream>
#include <c++/cassert>

namespace sf{

class Sprite;
class Text;

}

class AnimatedSprite;

template <typename T>
std::string toString(const T& value);

std::string toString(sf::Keyboard::Key key);

void centerOrigin(sf::Sprite& sprite);
void centerOrigin(AnimatedSprite& sprite);
void centerOrigin(sf::Text& text);

float toDegree(float radian);
float toRadian(float degree);

int randomInt(int exclusiveMax);

sf::Vector2f unitVector(sf::Vector2f vector);
float length(sf::Vector2f vector);

template <typename T>
std::string toString(const T& value)
{
    std::stringstream stream;
    stream << value;
    return stream.str();
}

#endif //SHOOTER_UTILITY_HPP
