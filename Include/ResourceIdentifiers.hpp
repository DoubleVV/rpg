//
// Created by DoubleVV on 26/07/2016.
//

#ifndef SHOOTER_RESOURCEIDENTIFIERS_HPP
#define SHOOTER_RESOURCEIDENTIFIERS_HPP

namespace sf
{
    class Texture;
    class Font;
}

namespace Textures
{
    enum ID
    {
        Entities,
        Jungle,
        TitleScreen,
        Buttons,
        Explosion,
        Particle,
        FinishLine,
        TileSet,
        SpriteSheet,
        SpriteSheetTest
    };
}

namespace Fonts
{
    enum ID
    {
        Arial,
        Impact,
        Main,
        Stats,
        Label
    };
}

template <typename Resource, typename Identifier>
class ResourceHolder;

typedef ResourceHolder<sf::Texture, Textures::ID> TextureHolder;
typedef ResourceHolder<sf::Font, Fonts::ID> FontHolder;


#endif //SHOOTER_RESOURCEIDENTIFIERS_HPP
