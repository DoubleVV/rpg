//
// Created by DoubleVV on 28/07/2016.
//

#ifndef SHOOTER_WORLD_HPP
#define SHOOTER_WORLD_HPP

#include <array>
#include <queue>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "../Third parties/json/src/json.hpp"

#include "SceneNode.hpp"
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SpriteNode.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"
#include "Character.hpp"
#include "MapIndentifier.hpp"

using json = nlohmann::json;

namespace sf{
    class RenderWindow;
}

class World : private sf::NonCopyable {

public:
    enum Layer{
        Background,
        Ground,
        Sprite,
        Air,
        LayerCount
    };

public:
    explicit World(sf::RenderWindow& window, FontHolder& fonts);
    void update(sf::Time dt);
    void draw();
    
    CommandQueue& getCommandQueue();
    
    bool hasAlivePlayer() const;
//    bool hasPlayerReachedEnd() const;

    int getPlayerCharacterHealth() const;
    int getPlayerCharacterMaxHealth() const;
    sf::Vector2f getPlayerCharacterPosition() const;

private:

    struct SpawnPoint{
        SpawnPoint(Character::Name name, float x, float y)
                : name(name),
                  x(x),
                  y(y) {

        }

        Character::Name name;
        float x;
        float y;
    };
    
    struct AnimatedTile{
        sf::Time sCurrentTime;
        int sCurrentTile;
        std::map<int, sf::Time> sFrameTime;
        std::vector<int> sFrames;
        sf::Vertex* sVertex;
    };

private:
    void loadTextures();
    void buildScene();
    void buildMap(Maps::ID mapName);
    void clearMap();
    void updateMap(sf::Time dt);
    
    void adaptPlayerVelocity();
    void adaptPlayerPosition();
    void adaptViewPosition();

//    void spawnEnemies();
//    void addEnemy(Aircraft::Type type, float relX, float relY);
//    void addEnemies();

    void spawnNpcs();
    void addNpc(Character::Name type, float relX, float relY);
    void addNpcs();
    
//    void guideMissiles();
    
    sf::FloatRect getBattlefieldBounds() const;
    sf::FloatRect getViewBounds() const;
    
    void handleCollisions();
//    void destroyEntitiesOutsideView();

private:
    sf::View mWorldView;
    sf::RenderWindow& mWindow;
    TextureHolder mTextures;
    FontHolder& mFonts;
    SceneNode mSceneGraph;
    std::array<SceneNode*, LayerCount> mSceneLayers;
    
    sf::FloatRect mWorldBounds;
    sf::Vector2f mSpawnPosition;
    Character* mPlayerCharacter;
    
    CommandQueue mCommandQueue;

    std::vector<Character*> mActiveEnemies;
    std::vector<SpawnPoint> mEnemySpawnPoints;
    std::vector<Character*> mActiveNpcs;
    std::vector<SpawnPoint> mNpcSpawnPoints;
    
    json mMapData;
    std::vector<AnimatedTile> mAnimatedTiles;
};

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2);

#endif //SHOOTER_WORLD_HPP
