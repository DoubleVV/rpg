//
// Created by DoubleVV on 27/07/2016.
//

#ifndef SHOOTER_SCENENODE_HPP
#define SHOOTER_SCENENODE_HPP


#include <c++/memory>
#include <c++/vector>

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>
#include <c++/set>

#include "Category.hpp"
#include "CommandQueue.hpp"

struct Command;

class SceneNode : public sf::Transformable,
                  public sf::Drawable,
                  private sf::NonCopyable {
public:
    typedef std::unique_ptr<SceneNode> Ptr;
    typedef std::pair<SceneNode*, SceneNode*> Pair;

public:
    SceneNode(Category::Type category = Category::None);
    
    void attachChild(Ptr child);
    Ptr detachChild(const SceneNode& node);
    
    void update(sf::Time dt, CommandQueue& commands);
    
    sf::Transform getWorldTransform() const;
    sf::Vector2f getWorldPosition() const;
    
    virtual sf::FloatRect getBoundingRect() const;
    virtual unsigned int getCategory() const;
    
    void checkSceneCollision(SceneNode& sceneGraph, std::set<Pair>& collisionPairs, sf::FloatRect worldBounds);
    void checkNodeCollision(SceneNode& node, std::set<Pair>& collisionPairs, sf::FloatRect worldBounds);
    
    virtual bool isMarkedForRemoval() const;
    virtual bool isDestroyed() const;
    
    void onCommand(const Command& command, sf::Time dt);
    
    void removeWrecks();

    void sortChildren();
    
private:
    std::vector<Ptr> mChildren;
    SceneNode* mParent;
    Category::Type mDefaultCategory;
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    void drawChildren(sf::RenderTarget& target, sf::RenderStates states) const;
    void drawBoundingRect(sf::RenderTarget& target, sf::RenderStates states) const;
    
    virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
    void updateChildren(sf::Time dt, CommandQueue& commands);
};

bool collision(const SceneNode& lhs, const SceneNode& rhs);
float distance(const SceneNode& lhs, const SceneNode& rhs);

#endif //SHOOTER_SCENENODE_HPP
