//
// Created by DoubleVV on 01/08/2016.
//

#ifndef SHOOTER_STATEIDENTIFIERS_HPP
#define SHOOTER_STATEIDENTIFIERS_HPP

namespace States{
    enum ID{
        None,
        Title,
        Menu,
        Game,
        Loading,
        Pause,
        Settings,
        GameOver
    };
}

#endif //SHOOTER_STATEIDENTIFIERS_HPP
