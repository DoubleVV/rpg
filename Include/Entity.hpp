//
// Created by DoubleVV on 27/07/2016.
//

#ifndef SHOOTER_ENTITY_HPP
#define SHOOTER_ENTITY_HPP

#include "SceneNode.hpp"
#include "CommandQueue.hpp"

class Entity : public SceneNode {
    
public:
    explicit Entity(int maxHealth);
    void repair(int points);
    void damage(int points);
    void destroy();

    int getHealth() const;
    int getMaxHealth() const;
    bool isDestroyed() const;
    
    void setVelocity(sf::Vector2f velocity);
    void setVelocity(float vx, float vy);
    void accelerate(sf::Vector2f velocity);
    void accelerate(float vx, float vy);
    sf::Vector2f getVelocity() const;

private:
    int mMaxHealth;
    int mHealth;
    sf::Vector2f mVelocity;

protected:
    virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
};


#endif //SHOOTER_ENTITY_HPP
