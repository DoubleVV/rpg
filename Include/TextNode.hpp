//
// Created by DoubleVV on 10/08/2016.
//

#ifndef SHOOTER_TEXTNODE_HPP
#define SHOOTER_TEXTNODE_HPP


#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>

#include "SceneNode.hpp"
#include "ResourceIdentifiers.hpp"
#include "ResourceHolder.hpp"

class TextNode : public SceneNode {
public:
    explicit TextNode(const FontHolder& fonts, const std::string& text);
    void setString(const std::string& text);
    
private:
    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    
private:
    sf::Text mText;
};


#endif //SHOOTER_TEXTNODE_HPP
